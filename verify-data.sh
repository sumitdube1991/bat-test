xmlPath=data/images/test
imagePath=data/images/test

xmlPath="${1:-$xmlPath}"
imagePath=${2:-$imagePath}
notFoundCounter=0
echo "Xml path:  $xmlPath"
echo "Image path:  $imagePath"
echo "Xml Files: " $(find $xmlPath -name "*.xml"  | wc -l)
echo "Image Files: " $(find $imagePath -name "*.jpg"  | wc -l)
echo "==================="

for file in $xmlPath/*.xml;
do
    filename=$(basename -- "$file")
    extension="${filename##*.}"
    name="${filename%.*}"
    imagename="$name.jpg"
    imageFullPath=$imagePath/$imagename 
    [ ! -f $imageFullPath ] && echo "$imageFullPath _________ NOT Found!" && ((notFoundCounter++))
done
echo "==================="
echo "Files Not Found: " $notFoundCounter
echo "...Finished"

