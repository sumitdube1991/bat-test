# BAT-AI project

This project consist of training, evaluation of models required for analysis of **BAT Data**. 

## Dependencies

-   Python 3.5.1 (and later versions)
-   Tensorflow 1.4.0 (and later versions)
-   OpenCV
-   Keras 2.x
-   ImageAI
    

## Installation
Once you have installed **python**, install other dependencies.

Install **tensorflow, keras** and  **opencv-python**,

		pip install -U tensorflow keras opencv-python  


Install **ImageAI**,

		pip3 install imageai --upgrade


## Run
Prepare training data and run following command in project directory,

		python3 train_model.py


