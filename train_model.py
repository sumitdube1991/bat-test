from imageai.Detection.Custom import DetectionModelTrainer

# lables = ['B&amp;HBG', 'B&amp;HPT','B&amp;HSF', 'B&amp;HSW', 'Capstan','Derby', 'DerbyStyle', 'Hollywood', 'JPGL', 'JPGLSPCL', 'JPGLSW','Nasirgold', 'Pilot', 'RoyalsGold', 'RoyalsLs', 'RoyalsNext' , 'SenorGold', 'StarFresh', 'StarNxt', 'Top10', 'ghw2', 'ghw']
lables = [
    'B&HBG', 
    'B&HPT',
    'B&HSF',
    'B&HSW',
    # 'Capstan',
    # 'Derby',
    # 'DerbyStyle',
    # 'Hollywood',
    # 'JPGL',
    # 'JPGLSPL',
    # 'JPGLSW',
    # 'NasirGold',
    # 'Pilot',
    # 'RoyalsGold',
    # 'RoyalsLs',
    # 'RoyalsNext',
    # 'SenorGold',
    # 'StarFresh',
    # 'StarNext',
    # 'Top10',
    # 'GHW2'
    ]
trainer = DetectionModelTrainer()
trainer.setModelTypeAsYOLOv3()
trainer.setDataDirectory(data_directory="data")
trainer.setTrainConfig(object_names_array=lables, batch_size=8, num_experiments=100, train_from_pretrained_model="pretrained-yolov3.h5")
trainer.trainModel()

